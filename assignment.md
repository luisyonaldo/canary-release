# SRE Assignment
## Background
Currently we don't have the possibility to do canary releases. To release a new version of an application, we simply perform a rolling update. Introducing this strategy will allow us to decrease potential chances of down-time, and increase developer confidence in regards to releases.
## Assignment
Design a release strategy to deploy new versions of micro-services to a Kubernetes environment. Keep in mind that developers who release a new version of their application need information about the newly released version.
## Requirements
- The canary release strategy should be applicable to a Kubernetes environment and supports running at least 2 versions simultaneously
- Current state of the deployment should be transparent and editable
- Provide code examples
- Have a working (runnable) example
## Additional information
- The current version of our Kubernetes cluster is 1.11+
- Most micro-services are running Spring boot in either Java or Kotlin (a simple
example of these sort of applications can be found [here](https://github.com/deBijenkorf/spring-cassandra-flux-api​))
- We currently use a combination of Helm charts and Ansible playbooks but we love to
see other/better ways
- Currently our CI/CD pipeline is set up with Jenkins, but again we love to see
other/better ways
- Feel free to ask any questions
