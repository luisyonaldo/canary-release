# Kubernetes Canary Releases
Kubernetes canary release deployment pipeline.
Designed to solve Bijenkorf [assignment](assignment.md).

## Requirements
- **Kubernetes cluster**: save a `base64` encoded version of the kubeconfig file in the `$KUBECONFIG_ENCODED` variable.
- **Istio**: needed for an advanced traffic management.

## Pipeline

### Build
Build the app using the `Dockerfile` and push it to gitlab container registry.

### Deploy
Deploy the application to kubernetes cluster base on branch name or tag, `prod` or `canary`:
- `prod`: deploy the production release of the application base on the manifest in `k8s/prod`.
- `canary`: deploy the production release of the application base on the manifest in `k8s/canary`.

There are multiple techniques to apply a canary release in kubernetes but the base idea is to target all deployed versions of an application and then apply some traffic management/routing.

The `service.yaml` manifest file inside `k8s/prod` is targeting all releases (k8s deployments) which allow us to do a Canary release using native kubernetes services load balancing, by adjusting the number of replicas each version has.

By using an Istio `VirtualService` we can introduce a more advanced traffic management using weights, cookies and/or headers, etc (more examples can be found [here](https://istio.io/docs/reference/config/istio.networking.v1alpha3/)).

### Testing

```
for i in `seq 4`; do \
  curl http://$INGRESS_GATEWAY/
  echo -e
done
1.0.0
1.0.0
1.0.0
1.0.0
```
